const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const users = require('./app/users');
const tracks_history = require('./app/tracks_history');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect('mongodb://localhost/lastFM',{useNewUrlParser: true}).then(()=>{
    app.use('/artists',artists);
    app.use('/albums',albums);
    app.use('/tracks',tracks);
    app.use('/tracks_history',tracks_history);
    app.use('/users',users);

    app.listen(port,()=> console.log(`Server runned on ${port} port`));
});