const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/lastFM',
    mongoOptions: {useNewUrlParser: true},
    facebook: {appId:'476182226453442',appSecret:'ac98a3fc552055aae625fef1b7bb2181'}
};