const nanoid = require ("nanoid");

const mongoose = require('mongoose');

const Artist =require('./models/Artist');
const Album =require('./models/Album');
const Track =require('./models/Track');
const User =require('./models/User');
const config = require('./config');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [artist, artist2,artist3] = await Artist.create(
        {
            name: "Queen",
            photo: "queen.png",
            info: "Legends",
            published: true
        },
        {
            name: "Shakira",
            photo: "shakira.jpg",
            info: 'Woman singer with strong voice',
            published: true
        },
        {
            name: "the Gazette",
            photo: "gazette.jpeg",
            info: "Japan J-rock group",
            published: true
        }
    );

    const [album, album2,album3,album4,album5] = await Album.create(
        {
            name:'Bohemian Rhapsody',
            artist: artist._id,
            year: '1975',
            cover_img: "BR_Cover.jpeg",
            published: true

        },
        {
            name: "Zootopia OST",
            artist: artist2._id,
            year: '2015',
            cover_img: "Z_Cover.jpg",
            published: true
        },
        {
            name: "Black Butler OST",
            artist: artist3._id,
            year: '2014',
            cover_img: "SH_Cover.jpeg",
            published: true
        },
        {
        name: "El Dorado",
        artist: artist2._id,
        year: '2017',
        cover_img: "Shakira-El-Dorado.jpg",
        published: true
        },
        {
        name: "TRACES VOL.2",
        artist: artist3._id,
        year: '2017',
        cover_img: "traces_vol_2.jpeg",
        published: true
        },
    );

    const [track, track2,track3,track4,track5] = await Track.create(
        {
            name:'Shiver',
            album: album3._id,
            long: 5,
            trackNo: 1,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Shiver (TV Ver)',
            album: album3._id,
            long: '1:30',
            trackNo: 2,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Hesitating Means Death',
            album: album3._id,
            long: '3:38',
            trackNo: 3,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Naraku',
            album: album3._id,
            long: '4:16',
            trackNo: 4,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Another Bite',
            album: album3._id,
            long: '4:44',
            trackNo: 5,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },

        {
            name:'Kareuta',
            album: album5._id,
            long: 5,
            trackNo: 1,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Cassic',
            album: album5._id,
            long: '6:54',
            trackNo: 2,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'D.L.N.',
            album: album5._id,
            long: '6:16',
            trackNo: 3,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Calm Envy',
            album: album5._id,
            long: '5:56',
            trackNo: 4,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },
        {
            name:'Reila',
            album: album5._id,
            long: '8:03',
            trackNo: 5,
            yt_link: "https://www.youtube.com/embed/gdHO7yobgUk",
            published: true
        },

        {
            name:'Bohemian Rhapsody',
            album: album._id,
            long: 6,
            trackNo: 1,
            yt_link:"https://www.youtube.com/embed/fJ9rUzIMcZQ",
            published: true
        },
        {
            name: 'I\'m in love with my car',
            album: album._id,
            long: 3,
            trackNo: 2,
            yt_link:"https://www.youtube.com/embed/oaEM4JYFPfw",
            published: true
        },

        {
            name:'Try Everything',
            album: album2._id,
            long: 4,
            trackNo: 1,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Stage Fright',
            album: album2._id,
            long: '0:39',
            trackNo: 2,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Grey\'s Uh-Mad At Me',
            album: album2._id,
            long: 2,
            trackNo: 3,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Ticket To Write',
            album: album2._id,
            long: 2,
            trackNo: 4,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Foxy Fakeout',
            album: album2._id,
            long: 2,
            trackNo: 5,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },

        {
            name:'Me Enamoré',
            album: album4._id,
            long: 4,
            trackNo: 1,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Nada',
            album: album4._id,
            long: 3,
            trackNo: 2,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Chantaje',
            album: album4._id,
            long: 3,
            trackNo: 3,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'When a Woman',
            album: album4._id,
            long: 3,
            trackNo: 4,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
        {
            name:'Amarillo',
            album: album4._id,
            long: 3,
            trackNo: 5,
            yt_link:"https://www.youtube.com/embed/c6rP-YP4c5I",
            published: true
        },
    );

    const [user1,user2] = await User.create(
        { username: 'TestUser',
          password: '1234',
          role: 'user',
          token: nanoid(),
          displayName:'TESTU'
        },
        { username: 'Admin',
          password: '2345',
          role: 'admin',
          token: nanoid(),
          displayName:'Admin'
        },
    );

    return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});