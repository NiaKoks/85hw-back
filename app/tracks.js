const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');
const Track = require('../models/Track');

const router = express.Router();

router.get('/',[tryAuth],(req,res)=>{

    let criteria = {published: true};

    if (req.user && req.user.role === 'admin'){
        criteria = {};
    }
    if (req.query.track) {
        criteria.track = req.query.track;
    }
    Track.find(criteria)
            .then(tracks => res.send(tracks))
            .catch(()=> res.sendStatus(500));
});
router.post('/',(req,res)=>{
    const track = new Track(req.body);
    track.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/published',[auth,permit('admin')],async (req,res) =>{
    try{
        const track = await Track.findById(req.params.id)
        if(!track) {res.sendStatus(404)}
        track.published = req.body.state;
        await track.save();
    } catch (e) {
        return res.status(500).send(e)
    }
    res.sendStatus(200)
});

router.delete('/:id', [auth,permit('admin')],(req,res)=>{

    Track.remove({_id: req.params.id})
        .then(result=>res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;