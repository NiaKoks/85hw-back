const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');
const path = require('path');
const nanoid = require('nanoid');
const multer = require('multer');

const config = require('../config')
const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',[tryAuth],(req,res)=>{
    let criteria = {published: true};

    if (req.user && req.user.role === 'admin'){
        criteria = {};
    }

    if (req.query.artist) {
        criteria.artist = req.query.artist;
    }

    Album.find(criteria).populate('artist')
            .then(albums => res.send(albums))
            .catch(()=>res.sendStatus(500))

});
router.post('/',[auth,upload.single('cover_img')],(req,res)=>{

    const albumData = req.body;

    if(req.file){
        albumData.cover_img = req.file.filename;
    }

    const album = new Album(albumData);
   album.save()
       .then(result => res.send(result))
       .catch(error => res.status(400).send(error));
});

router.post('/:id/published',[auth,permit('admin')],async (req,res) =>{
    try{
        const album = await Album.findById(req.params.id)
        if(!album) {res.sendStatus(404)}
        album.published = req.body.state;
        await album.save();
    } catch (e) {
        return res.status(500).send(e)
    }
    res.sendStatus(200)
});

router.delete('/:id', [auth,permit('admin')],(req,res)=>{
    Album.remove({_id: req.params.id})
        .then(result=>res.send(result))
        .catch(error=>res.status(403).send(error))

});


module.exports = router;