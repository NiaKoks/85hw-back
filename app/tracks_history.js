const express = require('express');
const User = require ('../models/User');
const TrackHistory = require('../models/TrackHistory');

const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', auth ,async (req,res)=>{

    const track_history = new TrackHistory({
        track: req.body.trackID,
        user: req.user._id,
        datetime: new Date().toISOString()
    });

    track_history.save()
        .then(result => res.sendStatus(200))
        .catch((e) => res.status(500).send(e));
});

router.get('/', auth, (req,res)=>{
        TrackHistory.find({user : req.user.id}).populate({path: 'track', populate: {path: 'album', populate: {path: 'artist'}}})
            .then(tracks => res.send(tracks))
            .catch(()=>res.sendStatus(500))
});
module.exports = router;