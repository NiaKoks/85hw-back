const express = require('express');
const path = require('path');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const nanoid = require('nanoid');
const multer = require('multer');
const Artist = require('../models/Artist');
const config = require("../config");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',[tryAuth],(req,res)=>{

    let criteria = {published: true};

    if (req.user && req.user.role === 'admin'){
        criteria = {};
    }
    Artist.find(criteria)
        .then(artists => res.send(artists))
        .catch(()=> res.sendStatus(500));
});

router.post('/',[auth,upload.single('photo')],(req,res)=>{

    const artistData = req.body;

    if (req.file) {
        artistData.photo = req.file.filename;
    }
    const artist = new Artist(artistData);
    artist.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/published',[auth,permit('admin')],async (req,res) =>{
    try{
      const artist = await Artist.findById(req.params.id)
        if(!artist) {res.sendStatus(404)}
        artist.published = req.body.state;
        await artist.save();
    } catch (e) {
        return res.status(500).send(e)
    }
    res.sendStatus(200)
});

router.delete('/:id', [auth,permit('admin')],(req,res)=>{
    Artist.remove({_id: req.params.id})
        .then(result=>res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;